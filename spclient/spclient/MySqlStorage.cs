﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace spclient
{
    class MySqlStorage : IDataStorage
    {
        private MySqlConnection connection;

        public MySqlStorage()
        {
            Initialize();
        }

        private void Initialize()
        {
            string connectionString = Properties.Settings.Default.MySqlConnectionString;
            connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        // TODO: Сделать вывод в лог-файл
                        // "Не могу подключиться к серверу"
                        break;

                    case 1045:
                        // TODO: Сделать вывод в лог-файл
                        // "Неправильный логин-пароль"
                        break;
                }
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch 
            {
                // TODO: Сделать вывод в лог-файл
                // MySqlException ex - "ex.Message"
                return false;
            }
        }

        public void SaveClient(Client client)
        {
            //TODO проверка на SQL-инъекцию
            try
            {
                string query = "INSERT INTO client (name) VALUES('" + client.name + "')";

                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }
            }
            catch
            {
                // TODO: Сделать вывод в лог-файл
                // MySqlException ex - "ex.Message"
            }
        }

        public void SaveService(Service service)
        {
            //TODO проверка на SQL-инъекцию
            try
            {
                string query = "INSERT INTO service (name) VALUES('" + service.name + "')";

                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }
            }
            catch
            {
                // TODO: Сделать вывод в лог-файл
                // MySqlException ex - "ex.Message"
            }
        }

        public void SaveInvoice(Invoice invoice)
        {
            //TODO проверка на SQL-инъекцию
            try
            {
                string cost = invoice.cost.ToString().Replace(',', '.');
                string query = 
                    "INSERT INTO service_invoice (client_id, service_id, cost) " +
                    " VALUES (" + invoice.client.id + ", " + invoice.service.id + ", " + cost + ")";

                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }
            }
            catch
            {
                // TODO: Сделать вывод в лог-файл
                // MySqlException ex - "ex.Message"
            }
        }

        public void SavePayment(Payment payment)
        {
            //TODO проверка на SQL-инъекцию
            try
            {
                string sum = payment.sum.ToString().Replace(',', '.');
                string query =
                    "INSERT INTO service_paid (client_id, service_id, sum) " +
                    " VALUES (" + payment.client.id + ", " + payment.service.id + ", " + sum + ")";

                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }
            }
            catch
            {
                // TODO: Сделать вывод в лог-файл
                // MySqlException ex - "ex.Message"
            }
        }

        public List<Client> GetClientsList()
        {
            List<Client> clients = new List<Client>();
            string query = "SELECT * FROM client";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                
                while (dataReader.Read())
                {
                    int clientId = Int16.Parse(dataReader["id"] + "");
                    string clientName = dataReader["name"] + "";
                    clients.Add(new Client(clientId, clientName));
                }

                dataReader.Close();

                this.CloseConnection();

                return clients;
            }
            else
            {
                return clients;
            }
        }

        public List<Service> GetServicesList()
        {
            List<Service> services = new List<Service>();
            string query = "SELECT * FROM service";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    int servicesId = Int16.Parse(dataReader["id"] + "");
                    string servicesName = dataReader["name"] + "";
                    services.Add(new Service(servicesId, servicesName));
                }

                dataReader.Close();

                this.CloseConnection();

                return services;
            }
            else
            {
                return services;
            }
        }

        public List<Invoice> GetAllIvoicesByClient(Client client)
        {
            List<Invoice> invoicesList = new List<Invoice>();
            string query = "SELECT * FROM service_invoice si WHERE si.client_id = " + client.id;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    int clientId  = Int16.Parse(dataReader["client_id"]  + "");
                    int serviceId = Int16.Parse(dataReader["service_id"] + "");
                    double cost = Double.Parse(dataReader["cost"] + "");
                    invoicesList.Add(
                        new Invoice(
                            client, 
                            new Service(serviceId), 
                            cost));
                }

                dataReader.Close();
                this.CloseConnection();
            }

            return invoicesList;
        }

        public List<Payment> GetAllPamentsByClient(Client client)
        {
            List<Payment> paymentsList = new List<Payment>();
            string query = "SELECT * FROM service_paid sp WHERE sp.client_id = " + client.id;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    int clientId = Int16.Parse(dataReader["client_id"] + "");
                    int serviceId = Int16.Parse(dataReader["service_id"] + "");
                    double cost = Double.Parse(dataReader["sum"] + "");
                    paymentsList.Add(
                        new Payment(
                            client,
                            new Service(serviceId),
                            cost));
                }

                dataReader.Close();
                this.CloseConnection();
            }

            return paymentsList;
        }
    }
}
