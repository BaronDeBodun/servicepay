﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spclient
{
    class Invoice
    {
        public int id;
        public Client client;
        public Service service;
        public double cost;

        public Invoice(Client cl, Service s, double co) { id = 0; client = cl; service = s; cost = co; }
        public Invoice(int i, Client cl, Service s, double co) { id = i; client = cl; service = s; cost = co; }

        public void Save()
        {
            MySqlStorage mss = new MySqlStorage();
            CommonStorage cs = new CommonStorage(mss);

            cs.SaveInvoice(this);
        }
    }
}
