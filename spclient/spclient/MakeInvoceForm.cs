﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spclient
{
    public partial class MakeInvoceForm : Form
    {
        public MakeInvoceForm()
        {
            InitializeComponent();
        }

        private void digit_press(object sender, KeyPressEventArgs e)
        {
            TextBox TBox = (TextBox)sender;
            if (!(Char.IsDigit(e.KeyChar)) && !((e.KeyChar == ',') && (TBox.Text.IndexOf(",") == -1) && (TBox.Text.Length != 0)))
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
        }

        private void saveInvoiceButton_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0 || comboBox2.SelectedIndex < 0 || textBox1.Text == "")
            {
                MessageBox.Show("Не заполнено одно из ключевых полей");
                return;
            }

            Invoice invoice = new Invoice(
                new Client(Int32.Parse(comboBox1.SelectedValue.ToString()), comboBox1.Text),
                new Service(Int32.Parse(comboBox2.SelectedValue.ToString()), comboBox2.Text),
                Convert.ToDouble(textBox1.Text)
                );

            invoice.Save();

            MessageBox.Show("Сохранено", "ok");
            this.Close();
        }

        private void MakeInvoceForm_Load(object sender, EventArgs e)
        {
            CommonStorage cs = new CommonStorage(new MySqlStorage());
            LoadClienstList(cs);
            LoadServicesList(cs);
        }

        private void LoadServicesList(CommonStorage cs)
        {
            List<Service> servicesList = cs.GetServicesList();
            comboBox2.DisplayMember = "Text";
            comboBox2.ValueMember = "Value";
            List<Object> items = new List<Object>();
            foreach (Service service in servicesList)
            {
                items.Add(new { Text = service.name, Value = service.id });
            }
            comboBox2.DataSource = items;
            comboBox2.SelectedIndex = 0;
        }

        private void LoadClienstList(CommonStorage cs)
        {
            List<Client> clientsList = cs.GetClientsList();
            comboBox1.DisplayMember = "Text";
            comboBox1.ValueMember = "Value";
            List<Object> items = new List<Object>();
            foreach (Client client in clientsList)
            {
                items.Add(new { Text = client.name, Value = client.id });
            }
            comboBox1.DataSource = items;
            comboBox1.SelectedIndex = 0;
        }

    }
}
