﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spclient
{
    class Client
    {
        public int id;
        public string name;

        public Client() { id = 0; name = ""; }
        public Client(string n) { id = 0; name = n; }
        public Client(int i, string n) { id = i; name = n; }

        public void Save()
        {
            MySqlStorage mss = new MySqlStorage();
            CommonStorage cs = new CommonStorage(mss);

            cs.SaveClient(this);
        }

    }
}
