﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spclient
{
    interface IDataStorage
    {
        void SaveClient(Client client);
        void SaveService(Service service);
        void SaveInvoice(Invoice invoice);
        void SavePayment(Payment payment);

        List<Client> GetClientsList();
        List<Service> GetServicesList();

        List<Invoice> GetAllIvoicesByClient(Client client);
        List<Payment> GetAllPamentsByClient(Client client);
    }
}
