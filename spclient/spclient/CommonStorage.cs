﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spclient
{
    class CommonStorage
    {
        public IDataStorage dataStorage { get; set; }
        public CommonStorage(IDataStorage ds)
        {
            dataStorage = ds;
        }

        public void SaveClient(Client client)
        {
            dataStorage.SaveClient(client);

        }

        public void SaveService(Service service)
        {
            dataStorage.SaveService(service);

        }

        public void SaveInvoice(Invoice invoice)
        {
            dataStorage.SaveInvoice(invoice);

        }

        public void SavePayment(Payment payment)
        {
            dataStorage.SavePayment(payment);

        }

        public List<Client> GetClientsList()
        {
            return dataStorage.GetClientsList();
        }

        public List<Service> GetServicesList()
        {
            return dataStorage.GetServicesList();
        }

        public List<Invoice> GetAllIvoicesByClient(Client client)
        {
            return dataStorage.GetAllIvoicesByClient(client);
        }

        public List<Payment> GetAllPaymentsByClient(Client client)
        {
            return dataStorage.GetAllPamentsByClient(client);
        }
    }
}
