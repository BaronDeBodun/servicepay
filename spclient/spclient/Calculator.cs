﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spclient
{
    class Calculator
    {
        public ICalculatePayment calcPayment { get; set; }

        public Calculator(ICalculatePayment cp)
        {
            calcPayment = cp;
        }

        public Dictionary<int, double> PerformPaymentCalculation(Client client, double clientPayment)
        {
            MySqlStorage mss = new MySqlStorage();
            CommonStorage cs = new CommonStorage(mss);

            // получаю список всех выставленных счетов клиенту
            List<Invoice> invoicesList = cs.GetAllIvoicesByClient(client); 
            Dictionary<int, double> costAllInvoices = GetClientInvoices(invoicesList);

            // получаю список всех оплаченых счетов клиента
            List<Payment> paymentsList = cs.GetAllPaymentsByClient(client);
            Dictionary<int, double> costAllPayments = GetClientPayments(paymentsList);

            // произвожу расчет задолженности клиета по каждой из выставленной услуге
            Dictionary<int, double> debtList = GetClientDebt(costAllInvoices, costAllPayments); 

            // Делаю расчет сумм платежек, согласно выбранной стратегии 
            return calcPayment.CalculatePayment(debtList, clientPayment);
        }

        public Dictionary<int, double> GetClientInvoices(List<Invoice> invoicesList)
        {
            Dictionary<int, double> costAllInvoices = new Dictionary<int, double>();
            foreach (Invoice invoice in invoicesList)
            {
                if (costAllInvoices.ContainsKey(invoice.service.id))
                    costAllInvoices[invoice.service.id] += invoice.cost;
                else
                    costAllInvoices.Add(invoice.service.id, invoice.cost);
            }
            return costAllInvoices;
        }

        public Dictionary<int, double> GetClientPayments(List<Payment> paymentsList)
        {
            Dictionary<int, double> costAllPayments = new Dictionary<int, double>();
            foreach (Payment payment in paymentsList)
            {
                if (costAllPayments.ContainsKey(payment.service.id))
                    costAllPayments[payment.service.id] += payment.sum;
                else
                    costAllPayments.Add(payment.service.id, payment.sum);
            }
            return costAllPayments;
        }

        public Dictionary<int, double> GetClientDebt(Dictionary<int, double> costAllInvoices, Dictionary<int, double> costAllPayments)
        {
            Dictionary<int, double> debtList = new Dictionary<int, double>();
            foreach (KeyValuePair<int, double> invoiceCost in costAllInvoices)
            {
                if (costAllPayments.ContainsKey(invoiceCost.Key))
                    debtList[invoiceCost.Key] = invoiceCost.Value - costAllPayments[invoiceCost.Key];
                else
                    debtList[invoiceCost.Key] = invoiceCost.Value;
            }
            return debtList;
        }
    }
}
