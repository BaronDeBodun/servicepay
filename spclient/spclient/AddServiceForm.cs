﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spclient
{
    public partial class AddServiceForm : Form
    {
        public AddServiceForm()
        {
            InitializeComponent();
        }

        private void saveServiceButton_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Название услуги не заполнено");
                return;
            }

            Service service = new Service(textBox1.Text);
            service.Save();

            MessageBox.Show("Сохранено", "ok");
            this.Close();
        }
    }
}
