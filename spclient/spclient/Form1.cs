﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spclient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RefreshTable();
        }

        private void AddClientMenuItem_Click(object sender, EventArgs e)
        {
            AddClientForm adf = new AddClientForm();
            adf.ShowDialog();
        }

        private void AddServiceMenuItem_Click(object sender, EventArgs e)
        {
            AddServiceForm asf = new AddServiceForm();
            asf.ShowDialog();
        }

        private void MakeInvoceMenuItem_Click(object sender, EventArgs e)
        {
            MakeInvoceForm mif = new MakeInvoceForm();
            mif.ShowDialog();
            RefreshTable();
        }

        private void AddPaymentMenuItem_Click(object sender, EventArgs e)
        {
            MakePaymentForm mpf = new MakePaymentForm();
            mpf.ShowDialog();
            RefreshTable();
        }

        private void CloseMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RefreshTable()
        {
            dataGridView1.Rows.Clear();
            CommonStorage cs = new CommonStorage(new MySqlStorage());
            List<Client> clientList = cs.GetClientsList();
            foreach (Client client in clientList)
            {
                ProportionalCalculation pc = new ProportionalCalculation();
                Calculator calc = new Calculator(pc);

                List<Invoice> invoicesList = cs.GetAllIvoicesByClient(client);
                Dictionary<int, double> costAllInvoices = calc.GetClientInvoices(invoicesList);

                List<Payment> paymentsList = cs.GetAllPaymentsByClient(client);
                Dictionary<int, double> costAllPayments = calc.GetClientPayments(paymentsList);

                Dictionary<int, double> debtList = calc.GetClientDebt(costAllInvoices, costAllPayments);

                foreach (KeyValuePair<int, double> debt in debtList)
                {
                    if (debt.Value != 0)
                    dataGridView1.Rows.Add(client.id, client.name, debt.Key, debt.Value);
                }
            }
        }
    }
}
