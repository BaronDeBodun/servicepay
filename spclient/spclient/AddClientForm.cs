﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spclient
{
    public partial class AddClientForm : Form
    {
        public AddClientForm()
        {
            InitializeComponent();
        }

        private void saveClientButton_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("ФИО не заполнено");
                return;
            }
            
            Client client = new Client(textBox1.Text);
            client.Save();

            MessageBox.Show("Сохранено", "ok");
            this.Close();
        }
    }
}
