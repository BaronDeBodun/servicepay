﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spclient
{
    interface ICalculatePayment
    {
        Dictionary<int, double> CalculatePayment(Dictionary<int, double> debtList, double clientPayment);
    }
}
