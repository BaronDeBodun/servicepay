﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spclient
{
    class Payment
    {
        public int id;
        public Client client;
        public Service service;
        public double sum;

        public Payment(Client cl, Service se, double su) { id = 0; client = cl; service = se; sum = su; }
        public Payment(int i, Client cl, Service se, double su) { id = i; client = cl; service = se; sum = su; }

        public void Save()
        {
            MySqlStorage mss = new MySqlStorage();
            CommonStorage cs = new CommonStorage(mss);

            cs.SavePayment(this);
        }
    }
}
