﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spclient
{
    class ProportionalCalculation : ICalculatePayment
    {
        /*
         *Класс, реализующий стратегию пропорционального расчета по задолженности
         */
        public Dictionary<int, double> CalculatePayment(Dictionary<int, double> debtList, double clientPayment)
        {
            double totalDebt = debtList.Sum(x => x.Value);
            Dictionary<int, double> sumPayments = new Dictionary<int, double>();

            if (totalDebt == 0) return sumPayments;

            foreach (KeyValuePair<int, double> debt in debtList)
            {
                if (debt.Value != 0)
                {
                    double sum = (double) debt.Value / totalDebt * clientPayment;
                    sumPayments.Add(debt.Key, Math.Round(sum, 2));                

                }
            }

            return sumPayments;
        }
    }
}
