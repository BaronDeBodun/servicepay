﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spclient
{
    public partial class MakePaymentForm : Form
    {
        public MakePaymentForm()
        {
            InitializeComponent();
        }

        private void digit_press(object sender, KeyPressEventArgs e)
        {
            TextBox TBox = (TextBox)sender;
            if (!(Char.IsDigit(e.KeyChar)) && !((e.KeyChar == ',') && (TBox.Text.IndexOf(",") == -1) && (TBox.Text.Length != 0)))
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
        }

        private void savePaymentButton_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0 || textBox1.Text == "")
            {
                MessageBox.Show("Не заполнено одно из ключевых полей");
                return;
            }

            ProportionalCalculation pc = new ProportionalCalculation();
            Calculator calc = new Calculator(pc);

            Dictionary<int, double> sumPayments = calc.PerformPaymentCalculation(
                new Client(Int32.Parse(comboBox1.SelectedValue.ToString()), comboBox1.Text), 
                Convert.ToDouble(textBox1.Text)
                );

            foreach (KeyValuePair<int, double> sum in sumPayments)
            {
                Payment payment = new Payment(
                    new Client(Int32.Parse(comboBox1.SelectedValue.ToString()), comboBox1.Text),
                    new Service(sum.Key),
                    sum.Value
                    );

                payment.Save();
            }

            MessageBox.Show("Сохранено", "ok");
            this.Close();
        }

        private void MakePaymentForm_Load(object sender, EventArgs e)
        {
            CommonStorage cs = new CommonStorage(new MySqlStorage());
            LoadClienstList(cs);
        }

        private void LoadClienstList(CommonStorage cs)
        {
            List<Client> clientsList = cs.GetClientsList();
            comboBox1.DisplayMember = "Text";
            comboBox1.ValueMember = "Value";
            List<Object> items = new List<Object>();
            foreach (Client client in clientsList)
            {
                items.Add(new { Text = client.name, Value = client.id });
            }
            comboBox1.DataSource = items;
            comboBox1.SelectedIndex = 0;
        }
    }
}
