﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spclient
{
    class Service
    {
        public int id;
        public string name;

        public Service() { id = 0; name = ""; }
        public Service(int i) { id = i; name = ""; }
        public Service(string n) { id = 0; name = n; }
        public Service(int i, string n) { id = i; name = n; }

        public void Save()
        {
            MySqlStorage mss = new MySqlStorage();
            CommonStorage cs = new CommonStorage(mss);

            cs.SaveService(this);
        }
    }
}
